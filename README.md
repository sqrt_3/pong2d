# Pong2D

A 2d version of the classic game Pong made with Unity and C#.

#Pong2D - Relive the old arcade classic

##What's new?
* 3 Scenes are in the game: The main menu scene, the game scene, and the credits scene.
* UI System includes: slider indicating how many goals have been scored so far relative to the amount of goals to win, each player's score, as well as Settings menu, etc.
* Players would start of on the main menu screen which would let them play the game, check out the credits, or quit the game.
* AI has been implemented: Game can now run on 1, 2 or 0 bots.
* AI is scaled according to difficulty, which can be changed at anytime by entering the settings menu whilst in-game (Esc)
* Music has been added into the Main & Credits scenes, as well as the Game scene.


####Credits
* Unity - software
* SF2 - Guile theme
* freesfx.co.uk - SFX
* Matias Lago - the game itself


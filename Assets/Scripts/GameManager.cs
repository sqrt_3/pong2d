﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public int m_NumGoalsToWin = 7;
    public float m_StartDelay = 3f;
    public float m_EndDelay = 3f;
    public Text m_MessageText;
    public GameObject m_PlayerPrefab;
    public GameObject m_BallPrefab;
    public PlayerManager[] m_Players;
    public BallManager m_Ball;
    public Slider m_ScoreSlider;
    public Text[] m_ScoreTexts;
    public AudioSource m_Music;
    public AudioClip m_Clip;
    public int[] m_AIDifficulty;
    public Slider[] m_DifficultySliders;
    public Canvas m_SettingsCanvas;
    public Toggle[] m_PlayerType;

    private int m_RoundNumber;
    private WaitForSeconds m_StartWait;
    private WaitForSeconds m_EndWait;
    private PlayerManager m_RoundWinner;
    private PlayerManager m_GameWinner;
    private float m_OriginalPitch;
    private float[] m_AIRange = { -1f, -1f };
    private float[] m_AIAccuracy = { -1f, -1f };
    private int[] m_AIIndex = { -1, -1};
    private int m_NumberOfAI = 0;
    private bool m_IsPaused = false;


    private void Start()
    {
        m_SettingsCanvas.enabled = false;
        UpdateAI();   
        m_OriginalPitch = m_Music.pitch;
        m_Music.clip = m_Clip;
        m_Music.Play();
        m_Music.loop = true;
        m_ScoreSlider.value = 0f;
        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);

        SpawnAllPlayers();
        SpawnBall();
        StartCoroutine(GameLoop());
    }

    private void UpdateAI()
    {
        m_NumberOfAI = 0;
        for (int i = 0; i < m_Players.Length; ++i)
        {
            if (!m_Players[i].m_IsPlayerHuman)
            {
                m_AIIndex[m_NumberOfAI] = i;
                m_NumberOfAI++;
            }
        }

        //make range and accuracy change according to the level of difficulty
        for (int i=0; i < m_NumberOfAI; ++i)
        {
            switch (m_AIDifficulty[i])
            {
                case 1: m_AIAccuracy[i] = 1f; m_AIRange[i] = 4f; break;
                case 2: m_AIAccuracy[i] = 0.99f; m_AIRange[i] = 4f; break;
                case 3: m_AIAccuracy[i] = 0.90f; m_AIRange[i] = 4f; break;
                case 4: m_AIAccuracy[i] = 0.8f; m_AIRange[i] = 4f; break;
                case 5: m_AIAccuracy[i] = 0.8f; m_AIRange[i] = 4.5f; break;
                case 6: m_AIAccuracy[i] = 0.8f; m_AIRange[i] = 4.5f; break;
                case 7: m_AIAccuracy[i] = 0.8f; m_AIRange[i] = 4.5f; break;
                case 8: m_AIAccuracy[i] = 0.8f; m_AIRange[i] = 5.8f; break;
                case 9: m_AIAccuracy[i] = 0.8f; m_AIRange[i] = 5.8f; break;
                case 10: m_AIAccuracy[i] = 0.8f; m_AIRange[i] = 6f; break;
            }
        }
        
       

        //m_AIRange = 1.05f * m_AIDifficulty;
        //m_AIAccuracy = 8f / m_AIDifficulty;
        
    }

    private void SpawnBall()
    {
        m_Ball.m_Instance = Instantiate(m_BallPrefab, m_Ball.m_SpawnPoint.position, m_Ball.m_SpawnPoint.rotation) as GameObject;
        m_Ball.Setup();
    }

    private void SpawnAllPlayers()
    {
        for(int i = 0; i < m_Players.Length; ++i)
        {
            m_Players[i].m_Instance = Instantiate(m_PlayerPrefab, m_Players[i].m_SpawnPoint.position,
                                                    m_Players[i].m_SpawnPoint.rotation) as GameObject;
            m_Players[i].m_PlayerNumber = i + 1;
            m_Players[i].Setup();
        }
    }

    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(RoundStarting());
        yield return StartCoroutine(RoundPlaying());
        yield return StartCoroutine(RoundEnding());

        if(m_GameWinner != null)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            StartCoroutine(GameLoop());
        }
    }

    private IEnumerator RoundStarting()
    {
        UpdateAI();
        SetScoreUI();
        ResetAllPlayers();
        DisablePlayerControl();
        m_Ball.DisableControl();
        
        ++m_RoundNumber;
        m_MessageText.text = "ROUND " + m_RoundNumber;
        yield return m_StartWait;
    }

    private IEnumerator RoundPlaying()
    {
        EnablePlayerControl();
        m_Ball.EnableControl();
        m_MessageText.text = string.Empty;

        while (Scored() == -1)
        {
            CheckForSettingsMenu();
            for (int i=0; i < m_NumberOfAI; ++i)
                NextMove(m_AIIndex[i]);
            yield return null;
        }
    }

    private IEnumerator RoundEnding()
    {
        DisablePlayerControl();
        m_Ball.DisableControl();
        m_RoundWinner = null;
        m_RoundWinner = GetRoundWinner();

        if (m_RoundWinner != null)
            ++m_RoundWinner.m_Wins;

        m_GameWinner = GetGameWinner();
        
        string message = EndMessage();
        m_MessageText.text = message;

        yield return m_EndWait;
    }

    private int Scored()
    {
        return m_Ball.OutOfBounds();
    }

    private PlayerManager GetRoundWinner()
    {
        if (Scored() >= 0)
            return m_Players[Scored()];
        else
            return null;
    }

    private PlayerManager GetGameWinner()
    {
        for(int i=0; i < m_Players.Length; ++i)
        {
            if (m_Players[i].m_Wins == m_NumGoalsToWin)
                return m_Players[i];
        }

        return null;
    }

    private string EndMessage()
    {
        string message = "DRAW!";

        if (m_RoundWinner != null)
            message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";

        message += "\n\n\n\n";

        for (int i = 0; i < m_Players.Length; i++)
        {
            message += m_Players[i].m_ColoredPlayerText + ": " + m_Players[i].m_Wins + " WINS\n";
        }

        if (m_GameWinner != null)
            message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";

        return message;
    }

    private void ResetAllPlayers()
    {
        for(int i=0; i < m_Players.Length; ++i)
        {
            m_Players[i].Reset();
        }
    }

    private void EnablePlayerControl()
    {
        for (int i = 0; i < m_Players.Length; ++i)
            m_Players[i].EnableControl();
    }

    private void DisablePlayerControl()
    {
        for (int i = 0; i < m_Players.Length; ++i)
            m_Players[i].DisableControl();
    }


    private void SetScoreUI()
    {
        m_ScoreSlider.value = (m_Players[0].m_Wins > m_Players[1].m_Wins) ? m_Players[0].m_Wins : m_Players[1].m_Wins;
        m_ScoreTexts[0].text = m_Players[0].m_ColoredPlayerText + ": " + m_Players[0].m_Wins;
        m_ScoreTexts[1].text = m_Players[1].m_ColoredPlayerText + ": " + m_Players[1].m_Wins;
    }

    public void NextMove(int playerIndex)
    {
        /*
            2 factors to AI difficulty: 
                - How close does the ball need to be for logic to be made from AI
                - Accuracy for how close it moves to ball's Y position
        */
        //Debug.Log("Player Index: " + playerIndex + ", Position: " + m_Players[playerIndex].m_Instance.transform.position);
        //if ball is within radius logic of paddle
        if (Mathf.Abs(Vector3.Distance(m_Players[playerIndex].m_Instance.transform.position, m_Ball.m_Instance.transform.position)) <= m_AIRange[playerIndex] )
        {
            //Move to the ball's y position when the ball is in range
            if(m_Ball.m_Instance.transform.position.y > m_Players[playerIndex].transform.position.y)
                m_Players[playerIndex].MoveTo(m_Ball.m_Instance.transform.position.y - m_AIAccuracy[playerIndex]);
            else
                m_Players[playerIndex].MoveTo(m_Ball.m_Instance.transform.position.y + m_AIAccuracy[playerIndex]);
        }


     }

    public void CheckForSettingsMenu()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            m_IsPaused = !m_IsPaused;
            Time.timeScale = (m_IsPaused) ? 1 : 0;
            m_SettingsCanvas.enabled = (m_IsPaused) ? false : true; 

            //Update difficulty and AI'ness of player depending on values of slider and toggle
            //if(!m_IsPaused)
           // {
                m_AIDifficulty[0] = (int)m_DifficultySliders[0].value;
                m_AIDifficulty[1] = (int)m_DifficultySliders[1].value;

                m_Players[0].m_IsPlayerHuman = m_PlayerType[0].isOn;
                m_Players[1].m_IsPlayerHuman = m_PlayerType[1].isOn;

                UpdateAI();

                DisablePlayerControl();
                EnablePlayerControl();

                UpdateAI();
            //}
        }
    }
}


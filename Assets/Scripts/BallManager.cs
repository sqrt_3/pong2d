﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class BallManager {


    [HideInInspector] public GameObject m_Instance;
    public Transform m_SpawnPoint;

    private BallMovement m_Movement;

	public void Setup()
    {
        m_Movement = m_Instance.GetComponent<BallMovement>();

        SpriteRenderer renderer = m_Instance.GetComponent<SpriteRenderer>();
        
    }

    public void DisableControl()
    {
        m_Movement.enabled = false;
    } 

    public void EnableControl()
    {
        m_Movement.enabled = true;
        m_Movement.ResetPosition();
        m_Movement.ApplyInitialForce();
    }

    public void Reset()
    {
        m_Instance.transform.position = m_SpawnPoint.position;
        m_Instance.transform.rotation = m_SpawnPoint.rotation;

        m_Instance.SetActive(false);
        m_Instance.SetActive(true);
    }

    public int OutOfBounds()
    {
        if(m_Movement.OutOfBounds())
        {
            if (m_Movement.transform.position.x > 0)
                return 0;
            else
                return 1;
        }
        return -1;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public int m_PlayerNumber = 1;
    public float m_VerticalSpeed = 0.2f;


    public Rigidbody2D m_Rigidbody;
    public string m_MovementAxisName;
    private float m_CurrentMovementValue = 0f;
    private float m_UpperScreenBound = 3f;
    private float m_LowerScreenBound = -3f;



    private void Awake()
    {
        //Get and store the reference of the rigidbody this script is attached to into var m_Rigidbody
        m_Rigidbody = GetComponent<Rigidbody2D>();
        //m_MovementAxisName = "Vertical" + m_PlayerNumber;

    }

    private void OnEnable()
    {
        //Ensure rigidbody is not kinematic (i.e physics apply to it)
        m_Rigidbody.isKinematic = false;
    }

    public void SetUpAxis()
    {
        //Prepare the correct axis name to check for input
        m_MovementAxisName = "Vertical" + m_PlayerNumber;
    }

    /*
    private void Start()
    {
        //Prepare the correct axis name to check for input
        m_MovementAxisName = "Vertical" + m_PlayerNumber;
    }
    */

    private void OnDisable()
    {
        //Ensure rigidbody is kinematic (i.e physics don't apply to it)
        m_Rigidbody.isKinematic = true;
    }

    private void FixedUpdate()
    {
        //Move the player
        Move();
    }

    private void Update()
    {
        //Get value of input the user is entering
        m_CurrentMovementValue = Input.GetAxis(m_MovementAxisName);
    }

    private void Move()
    {
        //move only if valid future position
        Vector2 movement = new Vector2(0, m_VerticalSpeed * m_CurrentMovementValue );
        float y = m_Rigidbody.transform.localPosition.y;
        if ( y + m_VerticalSpeed * m_CurrentMovementValue <= m_UpperScreenBound && y + m_VerticalSpeed * m_CurrentMovementValue >= m_LowerScreenBound)
        {
            m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
        }
    }

    public void MoveTo(float dst)
    {
        Vector2 v = new Vector2(m_Rigidbody.position.x, dst);
        m_Rigidbody.MovePosition(m_Rigidbody.position + v);
    }
}

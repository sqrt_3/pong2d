﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public AudioSource m_Audio;
    public AudioClip m_MusicClip;

    private float m_OriginalPitch;

	// Use this for initialization
	void Start () {
        m_OriginalPitch = m_Audio.pitch;
        MusicAudio();
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    private void MusicAudio()
    {
        m_Audio.clip = m_MusicClip;
        m_Audio.pitch = m_OriginalPitch;
        m_Audio.Play();
    }

    public void LoadGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ShowCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}

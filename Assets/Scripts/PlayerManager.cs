﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


//[Serializable]
public class PlayerManager : MonoBehaviour{

    public Color m_PlayerColor;
    public Transform m_SpawnPoint;
    public bool m_IsPlayerHuman;

    [HideInInspector] public int m_PlayerNumber;
    [HideInInspector] public string m_ColoredPlayerText;
    [HideInInspector] public GameObject m_Instance;
    [HideInInspector] public int m_Wins;
   

    [HideInInspector] public PlayerMovement m_Movement;

    public void Setup()
    {
        
        m_Movement = m_Instance.GetComponent<PlayerMovement>();

        m_Movement.m_PlayerNumber = m_PlayerNumber;

        m_ColoredPlayerText = "<color=#" + ColorUtility.ToHtmlStringRGB(m_PlayerColor) + ">PLAYER " + m_PlayerNumber + "</color>";

        SpriteRenderer renderer = m_Instance.GetComponent<SpriteRenderer>();
        m_Movement.SetUpAxis();
    }

    public void DisableControl()
    {
        m_Movement.enabled = false;
    }

    public void EnableControl()
    {
        //Disable movement axis for a player that is not human
        if (!m_IsPlayerHuman)
        {
            m_Movement.m_MovementAxisName = "Vertical";
            Debug.Log("Axis of movement now restricted for AI!");
        }
        else
        {
            m_Movement.m_MovementAxisName = "Vertical" + m_PlayerNumber;
            Debug.Log("Axis of movement enabled for player!");
        }

        Debug.Log("IsPlayerHuman?: " + m_IsPlayerHuman);
        m_Movement.enabled = true;

    }

    public void Reset()
    {
        m_Instance.transform.position = m_SpawnPoint.position;
        m_Instance.transform.rotation = m_SpawnPoint.rotation;

        m_Instance.SetActive(false);
        m_Instance.SetActive(true);
    }

    public void MoveTo(float dst)
    {
        m_Instance.transform.position = new Vector3(m_Instance.transform.position.x, dst, 0);
    }


}

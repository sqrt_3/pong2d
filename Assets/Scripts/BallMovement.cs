﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour {


    private Rigidbody2D m_Rigidbody = null;
    private float m_HorizontalBound = 10.6f;
    private float m_VerticalBound = 5.0f;
    private float m_NetForceX = 0f;
    private float m_NetForceY = 0f;
    private float m_Scalar = 200f;

    /*
    private enum eGoal
    {
        NONE,
        PLAYER_1,
        PLAYER_2
    };
    */

    private void Awake()
    {
        //Get and store the reference of the rigidbody this script is attached to into var m_Rigidbody
        m_Rigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        //Ensure rigidbody is not kinematic (i.e physics apply to it)
        m_Rigidbody.isKinematic = false;
    }

    private void Start()
    {

    }

    private void OnDisable()
    {
        //Ensure rigidbody is kinematic (i.e physics don't apply to it)
        m_Rigidbody.isKinematic = true;
    }

    private void FixedUpdate()
    {
        //CheckGoal();
        /*
        if (OutOfBounds())
        {
            ResetPosition();
            ApplyInitialForce();
        }
            */
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        //collided with a player
        if(collision.tag == "Player")
        {
            Debug.Log("Collider with player at " + transform.position);
            //apply force to rotate/move

            //METHOD B: Opposite angle
            Vector2 opposite = new Vector2(- 2 * m_NetForceX, -m_Scalar);
            m_Rigidbody.AddForceAtPosition(opposite, m_Rigidbody.position);

            m_NetForceX -= 2 * m_NetForceX;
            m_NetForceY -= m_Scalar;


            Debug.Log(m_NetForceX + ", " + m_NetForceY);
        }
        //collided with a wall
        else if(collision.tag == "Wall")
        {
            Debug.Log("Collider with wall at " + transform.position);
            //apply force to rotate/move

            //METHOD A: Complementing

            //if Dx < 0 (heading left), this code works
            if(m_NetForceX < 0)
            {
                Vector2 opposite = new Vector2(-m_Scalar, -2 * m_NetForceY);
                m_Rigidbody.AddForceAtPosition(opposite, m_Rigidbody.position);

                m_NetForceX -= m_Scalar;
                m_NetForceY -= 2 * m_NetForceY;
            }
            //if Dx > 0 (heading right), this code works
            else if (m_NetForceX > 0)
            {
                Vector2 opposite = new Vector2(m_Scalar, -2 * m_NetForceY);
                m_Rigidbody.AddForceAtPosition(opposite, m_Rigidbody.position);

                m_NetForceX += m_Scalar;
                m_NetForceY -= 2 * m_NetForceY;

            }

            Debug.Log(m_NetForceX + ", " + m_NetForceY);
        }
    }


    /*
     * Out of bounds if:
     * - X position < -10.6 OR X position > 10.6
     * - Y position < -5 OR Y position > 5
     * */
    public bool OutOfBounds()
    {
        return (Mathf.Abs(transform.localPosition.x) >= m_HorizontalBound || 
                Mathf.Abs(transform.localPosition.y) >= m_VerticalBound);
    }
    
    public void ResetPosition()
    {

        //Stop/Cancel out vector code - effectively freezes ball
        Debug.Log("Before Position reset! Net forces: " + m_NetForceX + ", " + m_NetForceY);
        Vector2 cancel = new Vector2(-m_NetForceX, -m_NetForceY);
        m_Rigidbody.AddForceAtPosition(cancel, m_Rigidbody.position);
        m_NetForceX = 0;
        m_NetForceY = 0;

        //Position at (0, 0)
        m_Rigidbody.transform.position = new Vector2(0f, 0f);
        
    }

    public void ApplyInitialForce()
    {
        float random = UnityEngine.Random.Range(-100, 100);
        float scalar = (random > 0) ? m_Scalar : -m_Scalar;

        m_Rigidbody.AddForceAtPosition(new Vector2(scalar, m_Scalar), m_Rigidbody.position);
        m_NetForceX += (scalar);
        m_NetForceY += m_Scalar;

        Debug.Log(m_NetForceX + ", " + m_NetForceY);
    }
    
}
